import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { ListGroup } from "react-bootstrap";
import "../index.css";

export default class MyResult extends Component {
  render() {
    return (
      <div>
        <ListGroup.Item style={{ textAlign: "left" }}>{this.props.output}</ListGroup.Item>
      </div>
    );
  }
}

