import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Card, Form, ListGroup } from "react-bootstrap";
import MyResult from "./MyResult";

export default class MyCalculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numbers : []
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    var num1 = Number(this.number1.value);
    var num2 = Number(this.number2.value);
    var op = this.option.value;
    var result;

    switch (op) {
      case "+":
        result = num1 + num2;
        break;
      case "-":
        result = num1 - num2;
        break;
      case "x":
        result = num1 * num2;
        break;
      case "/":
        result = num1 / num2;
        break;
      case "%":
        result = num1 % num2;
        break;
      default:
        alert("Please choose your operator");
        break;
    }

    this.setState({
        numbers: this.state.numbers.concat(result),
    });
  }

  render() {

    var item = this.state.numbers.map((ele, index) => (
        <MyResult output={ele} key={index} />
      ));

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="container App">
            <div className="row pt-4">
              <div className="col-md-4">
                <Card>
                  <Card.Img variant="top" src={require('../Image/pic.png')} />
                  <Card.Body>
                    <Form.Group>
                      <Form.Control
                        type="number"
                        ref={(num1) => (this.number1 = num1)}
                        name="number1"
                      />
                    </Form.Group>

                    <Form.Group>
                      <Form.Control
                        type="number"
                        ref={(num2) => (this.number2 = num2)}
                        name="number2"
                      />
                    </Form.Group>

                    <Form.Group controlId="">
                      <Form.Control as="select" custom ref={(choose) => (this.option = choose)} name="option">
                        <option value="+">+  Plus</option>
                        <option value="-">-  Subtraction</option>
                        <option value="x">x  Multiplication</option>
                        <option value="/">/  Division</option>
                        <option value="%">%  Module</option>
                      </Form.Control>
                    </Form.Group>
                    <Button variant="primary" type="submit" style={{ textAlign: "right" }}>
                      Calculate
                    </Button>
                  </Card.Body>
                </Card>
              </div>

              <div className="col-md-4">
              <h1>Result history</h1> 
              <br/>
              <ListGroup>{item}</ListGroup>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
